#!/usr/bin/python2.7
# coding=utf-8

from __future__ import unicode_literals
import sys
import codecs
import string

"""Упражнение "Количество слов"

Функция main() ниже уже определена и заполнена. Она вызывает функции 
print_words() и print_top(), которые вам нужно заполнить.

1. Если при вызове файла задан флаг --count, вызывается функция 
print_words(filename), которая подсчитывает, как часто каждое слово встречается 
в тексте и выводит:
слово1 количество1
слово2 количество2
...

Выводимый список отсортируйте в алфавитном порядке. Храните все слова 
в нижнем регистре, т.о. слова "Слон" и "слон" будут обрабатываться как одно 
слово.

2. Если задан флаг --topcount, вызывается функция print_top(filename),
которая аналогична функции print_words(), но выводит только топ-20 наиболее 
часто встречающихся слов, таким образом первым будет самое часто встречающееся 
слово, за ним следующее по частоте и т.д.

Используйте str.split() (без аргументов), чтобы разбить текст на слова.

Отсекайте знаки припинания при помощи str.strip() с знаками припинания 
в качестве аргумента.

Совет: не пишите всю программу сразу. Доведите ее до какого-то промежуточного 
состояния и выведите вашу текущую структуру данных. Когда все будет работать 
как надо, перейдите к следующему этапу.

Дополнительно: определите вспомогательную функцию, чтобы избежать дублирования 
кода внутри print_words() и print_top().

"""


# +++ваш код+++
# Определите и заполните функции print_words(filename) и print_top(filename).
# Вы также можете написать вспомогательную функцию, которая читает файл,
# строит по нему словарь слово/количество и возвращает этот словарь.
# Затем print_words() и print_top() смогут просто вызывать эту вспомогательную функцию.

###

# Это базовый код для разбора аргументов коммандной строки.
# Он вызывает print_words() и print_top(), которые необходимо определить.

def get_words_dictionary(file1):
    f1 = codecs.open(file1, encoding='utf-8')
    list_w = f1.read().split(' ')
    word_dictionary = {}
    # Словарь
    string_strip = string.punctuation + ' '
    for x in list_w:
        x = x.strip(string_strip).lower()
        if x:
            word_dictionary[x] = word_dictionary.get(x, 0) + 1
    return word_dictionary


def print_words(file1):
    word_dictionary = get_words_dictionary(file1)
    for k in sorted(word_dictionary.keys()):
        print '{0:25} {1:3}'.format(k, word_dictionary[k])


def print_top(file1):
    word_dictionary = get_words_dictionary(file1)
    for k, v in sorted(word_dictionary.items(), key=lambda x: x[1], reverse=True)[:25]:
        print '{0:25} {1:3}'.format(k, v)


def main():
    if len(sys.argv) != 3:
        print('usage: python wordcount.py {--count | --topcount} file')
        sys.exit(1)

    option = sys.argv[1]
    file1 = sys.argv[2]
    if option == '--count':
        print_words(file1)
    elif option == '--topcount':
        print_top(file1)
    else:
        print('unknown option: ' + option)
    sys.exit(1)


if __name__ == '__main__':
    main()
